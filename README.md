# CorePDF
A PDF viewer for C Suite.
It is based on [Poppler](https://poppler.freedesktop.org/).

<img src="corepdf.png" width="500">

### Download
You can download latest release.
* [Source](https://gitlab.com/cubocore/coreapps/corepdf/tags)
* [ArchPackages](https://gitlab.com/cubocore/wiki/tree/master/ArchPackages)
* [DebPackages](https://gitlab.com/cubocore/wiki/-/tree/master/DebPackages)
* [Gentoo](https://gitweb.gentoo.org/repo/proj/guru.git/tree/gui-apps/corepdf)
* [AppImages](https://gitlab.com/cubocore/wiki/-/tree/master/AppImages)
* [Flatpak](https://flathub.org/apps/details/cc.cubocore.CorePDF)

### Dependencies:
* qt6
* [libcprime](https://gitlab.com/cubocore/libcprime)
* [QDocumentView](https://gitlab.com/extraqt/qdocumentview)
* poppler-qt6
* cups (optional)
* djvulibre (optional)

### Information
Please see the [Wiki page](https://gitlab.com/cubocore/wiki) for this info.
* Changes in latest release ([ChangeLog](https://gitlab.com/cubocore/wiki/-/blob/master/ChangeLog))
* Build from the source ([BuildInfo](https://gitlab.com/cubocore/wiki/blob/master/BuildInfo.md))
* Tested In ([Test System](https://gitlab.com/cubocore/wiki/blob/master/TestSystem))
* Known Bugs ([Current list of issues](https://gitlab.com/groups/cubocore/coreapps/-/issues))
* Help Us

### Feedback
* We need your feedback to improve the C Suite. Send us your feedback through GitLab [issues](https://gitlab.com/groups/cubocore/coreapps/-/issues).
  
  Or feel free to join and chat with us in IRC/Matrix #cubocore:matrix.org or [Element.io](https://app.element.io/#/room/#cubocore:matrix.org)
