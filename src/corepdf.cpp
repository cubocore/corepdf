/**
  * This file is a part of CorePDF.
  * A PDF viewer for C Suite.
  * Copyright 2019 CuboCore Group
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 3 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, vsit http://www.gnu.org/licenses/.
  **/


#include "corepdf.h"
#include "ui_corepdf.h"

#include <QListWidgetItem>
#include <QLabel>
#include <QBasicTimer>

#include <cprime/cprime.h>
#include <cprime/pinit.h>
#include <cprime/shareit.h>
#include <cprime/appopenfunc.h>
#include <cprime/messageengine.h>
#include <cprime/pinmanage.h>

#include <QDocument.hpp>
#include <QDocumentNavigation.hpp>
#include <QDocumentView.hpp>
#include <PopplerDocument.hpp>

#include "settings.h"

corepdf::corepdf(QWidget *parent) : QWidget(parent)
	, ui(new Ui::corepdf)
	, smi(new settings)
{
	ui->setupUi(this);

	// Set the transparent color to the main widget
	QPalette p(palette());

	p.setColor(QPalette::Base, Qt::transparent);
	this->setPalette(p);

	loadSettings();
    startsetup();
    setupPage();
	setupIcons();
}


corepdf::~corepdf()
{
	delete PdfWidget;
	delete ui;
	delete smi;
}


/**
  * @brief Setup ui elements
  */
void corepdf::startsetup()
{
	PdfWidget = new QDocumentView(this);
    PdfWidget->setZoomFactor(1);
    PdfWidget->setShowToolsOSD(false);
    ui->u->addWidget(PdfWidget, 0, 0);

	// all toolbuttons icon size in sideView
	QList<QToolButton *> toolBtns = ui->sideView->findChildren<QToolButton *>();

	for (QToolButton *b: toolBtns)
	{
		if (b)
		{
			b->setIconSize(toolsIconSize);
		}
	}

	// all toolbuttons icon size in toolBar
	QList<QToolButton *> toolBtns2 = ui->toolBar->findChildren<QToolButton *>();

	for (QToolButton *b: toolBtns2)
	{
		if (b)
		{
			b->setIconSize(toolsIconSize);
		}
	}

    // all toolbuttons icon size in searchB
    QList<QToolButton *> toolBtns3 = ui->searchB->findChildren<QToolButton *>();

    for (QToolButton *b: toolBtns3)
    {
        if (b)
        {
            b->setIconSize(toolsIconSize);
        }
    }

	shotcuts();

	ui->sideView->setVisible(false);
	ui->menu->setEnabled(true);

	ui->appTitle->setAttribute(Qt::WA_TransparentForMouseEvents);
	ui->appTitle->setFocusPolicy(Qt::NoFocus);
	this->resize(800, 500);


	ui->searchB->setVisible(false);

	if (uiMode == 2)
	{
		// setup mobile UI

		this->setWindowState(Qt::WindowMaximized);

		ui->activitiesList->setVisible(false);
		ui->fileName->setVisible(false);

		ui->activitiesList->setVisible(false);
	}
	else
	{
		// setup desktop or tablet UI

		if (windowMaximized)
		{
			this->setWindowState(Qt::WindowMaximized);
			qDebug() << "window is maximized";
		}
		else
		{
			this->resize(windowSize);
		}

		if (activities)
		{
			loadActivities();
		}
	}

	ui->page->setCurrentIndex(0);

	// Search counter
	searchCountLbl = new QLabel(this);
	searchCountLbl->setStyleSheet("QLabel{ margin: 5px; padding: 5px; background-color: rgba(0, 0, 0, 0.5); border-radius: 3px; }");
	searchCountLbl->setText("Search 0 of 0");
	ui->u->addWidget(searchCountLbl, 0, 0, Qt::AlignRight | Qt::AlignTop);
	searchCountLbl->hide();

	connect(PdfWidget, &QDocumentView::searchHighlightChanged, this, &corepdf::updateSearchLabel);

	connect(
		PdfWidget, &QDocumentView::matchesFound, [ = ](int numMatches) {
		if (numMatches and not searchCountLbl->isVisible())
		{
			searchCountLbl->setText("Searching...");
			searchCountLbl->show();
		}

		else
		{
			searchCountLbl->hide();
		}
	}
		);

	connect(
		PdfWidget, &QDocumentView::searchComplete, [ = ](int) {
		updateSearchLabel();
	}
		);

	connect(ui->jumpToPage, &QToolButton::clicked, this, &corepdf::jumpPage);
	connect(ui->addFiles, SIGNAL(clicked()), this, SLOT(openFileDialog()));
	connect(ui->open, SIGNAL(clicked()), this, SLOT(openFileDialog()));


	/**
	 * For desktops: uiMode == 0.
	 * If it's not a desktop, enable QScroller.
	 */
	if (uiMode != 0)
	{
		QScroller::grabGesture(PdfWidget, QScroller::LeftMouseButtonGesture);
	}

	delayTimer = new QBasicTimer();

	connect(ui->searchHere, &QLineEdit::textEdited, [ = ]() {
		delayTimer->start(500, Qt::PreciseTimer, this);
	});

	connect(ui->closeSearch, &QToolButton::clicked, [ = ]() {
		ui->searchHere->clear();
		PdfWidget->searchText(QString());
	});
}


/**
  * @brief Loads application settings
  */
void corepdf::loadSettings()
{
	// get CSuite's settings
	toolsIconSize    = smi->getValue("CoreApps", "ToolsIconSize");
	uiMode           = smi->getValue("CoreApps", "UIMode");
	activities       = smi->getValue("CoreApps", "KeepActivities");
	listViewIconSize = smi->getValue("CoreApps", "ListViewIconSize");

	// get app's settings
    pageFit         = smi->getValue("CorePDF", "PageFit");
	pageLayout      = smi->getValue("CorePDF", "PageLayout");
    pageView        = smi->getValue("CorePDF", "PageView");
	windowSize      = smi->getValue("CorePDF", "WindowSize");
	windowMaximized = smi->getValue("CorePDF", "WindowMaximized");
}


void corepdf::setupIcons()
{
	ui->open->setIcon(CPrime::ThemeFunc::themeIcon("document-open-symbolic", "quickopen-file", "document-open-symbolic"));
	ui->rotate->setIcon(CPrime::ThemeFunc::themeIcon("object-rotate-right-symbolic", "object-rotate-right", "object-rotate-right"));
	ui->pinIt->setIcon(CPrime::ThemeFunc::themeIcon("bookmark-new-symbolic", "bookmark-new-symbolic", "bookmark-new"));
	ui->shareIt->setIcon(CPrime::ThemeFunc::themeIcon("document-send-symbolic", "document-send-symbolic", "document-send"));
	ui->savePageAsPic->setIcon(CPrime::ThemeFunc::themeIcon("document-save-symbolic", "document-save", "document-save"));
	ui->openmetadataApp->setIcon(CPrime::ThemeFunc::themeIcon("document-properties-symbolic", "document-properties", "document-properties"));
	ui->search->setIcon(CPrime::ThemeFunc::themeIcon("edit-find-symbolic", "edit-find", "edit-find"));
	ui->jumpToPage->setIcon(CPrime::ThemeFunc::themeIcon("view-refresh-symbolic", "view-refresh", "view-refresh"));
	ui->menu->setIcon(CPrime::ThemeFunc::themeIcon("open-menu-symbolic", "application-menu", "open-menu"));
	ui->previousW->setIcon(CPrime::ThemeFunc::themeIcon("go-previous-symbolic", "go-previous", "go-previous"));
	ui->nextW->setIcon(CPrime::ThemeFunc::themeIcon("go-next-symbolic", "go-next", "go-next"));
	ui->closeSearch->setIcon(CPrime::ThemeFunc::themeIcon("window-close-symbolic", "window-close", "window-close"));
}


/**
  * @brief Loads application shotcuts
  */
void corepdf::shotcuts()
{
	QShortcut *shortcut;

	shortcut = new QShortcut(QKeySequence(Qt::CTRL | Qt::Key_O), this);
	connect(shortcut, &QShortcut::activated, this, &corepdf::openFileDialog);
	shortcut = new QShortcut(QKeySequence(Qt::CTRL | Qt::Key_F), ui->app);
	connect(shortcut, &QShortcut::activated, this, &corepdf::on_search_clicked);
	shortcut = new QShortcut(QKeySequence(Qt::CTRL | Qt::SHIFT | Qt::Key_F), ui->app);
	connect(shortcut, &QShortcut::activated, this, &corepdf::on_closeSearch_clicked);
}


/**
  * @brief Sends file to library
  */
void corepdf::loadFile(const QString& file)
{
	workFilePath = file;
	ui->page->setCurrentIndex(1);

	/** This will do the loading */
	PdfDoc = PdfWidget->load(workFilePath);
	PdfWidget->setFocus();

    ui->fileName->setText( QFileInfo(workFilePath).fileName());

}

void corepdf::setupPage()
{
    if ( pageLayout == 0 ){
        ui->pageLayout->setText("Page Layout: Single");
        ui->pageLayout->setIcon(CPrime::ThemeFunc::themeIcon( "view-pages-single-symbolic", "view-pages-single", "view-pages-single"));
        PdfWidget->setPageLayout(QDocumentView::SinglePage);
    } else if ( pageLayout == 1 ){
        ui->pageLayout->setText("Page Layout: Facing");
        ui->pageLayout->setIcon(CPrime::ThemeFunc::themeIcon( "view-pages-facing-symbolic", "view-pages-facing", "view-pages-facing"));
        PdfWidget->setPageLayout(QDocumentView::FacingPages);
    } else if ( pageLayout == 2 ){
        ui->pageLayout->setText("Page Layout: Book");
        ui->pageLayout->setIcon(CPrime::ThemeFunc::themeIcon( "view-pages-facing-first-centered-symbolic", "view-pages-facing-first-centered", "view-pages-facing-first-centered"));
        PdfWidget->setPageLayout(QDocumentView::BookView);
    }

    if ( pageFit == 0 ){
        ui->pageFit->setText("Page Fit: Height");
        ui->pageFit->setIcon(CPrime::ThemeFunc::themeIcon( "zoom-fit-height-symbolic", "zoom-fit-height", "zoom-fit-height"));
        PdfWidget->setZoomMode(QDocumentView::FitInView);
    } else if ( pageFit == 1 ){
        ui->pageFit->setText("Page Fit: Width");
        ui->pageFit->setIcon(CPrime::ThemeFunc::themeIcon( "zoom-fit-width-symbolic", "zoom-fit-width", "zoom-fit-width"));
        PdfWidget->setZoomMode(QDocumentView::FitToWidth);
    } else if ( pageFit == 2 ){
        ui->pageFit->setText("Page Fit: Custom");
        ui->pageFit->setIcon(CPrime::ThemeFunc::themeIcon( "zoom-fit-page-symbolic", "zoom-fit-page", "zoom-fit-page"));
        PdfWidget->setZoomMode(QDocumentView::CustomZoom);
        PdfWidget->setZoomFactor(1);
    }

    if ( pageView == 0 ){
        ui->pageView->setText("Page View: Single");
        ui->pageView->setIcon(CPrime::ThemeFunc::themeIcon( "view-pages-single-symbolic", "view-pages-single", "view-pages-single"));
        PdfWidget->setLayoutContinuous(true);
    } else if ( pageView == 1 ){
        ui->pageView->setText("Page View: Continuous");
        ui->pageView->setIcon(CPrime::ThemeFunc::themeIcon( "view-pages-continuous-symbolic", "view-pages-continuous", "view-pages-continuous"));
        PdfWidget->setLayoutContinuous(false);
    }


    if (not PdfWidget){
        return;
    }

    if (not PdfWidget->document() or PdfWidget->document()->status() != QDocument::Ready){
        return;
    }

    PdfWidget->update();
}




void corepdf::closeEvent(QCloseEvent *event)
{
	if (not workFilePath.isEmpty() && activities)
	{
		CPrime::ActivitiesManage::saveToActivites("corepdf", QStringList() << workFilePath);
	}

	smi->setValue("CorePDF", "WindowSize", this->size());
	smi->setValue("CorePDF", "WindowMaximized", this->isMaximized());

	event->accept();
}


void corepdf::timerEvent(QTimerEvent *tEvent)
{
	if (delayTimer->timerId() == tEvent->timerId())
	{
		delayTimer->stop();
		PdfWidget->searchText(ui->searchHere->text());
	}

	QWidget::timerEvent(tEvent);
}


void corepdf::loadActivities()
{
	if (not activities)
	{
		ui->activitiesList->setVisible(false);
		return;
	}

	ui->activitiesList->clear();

	QSettings   recentActivity(CPrime::Variables::CC_ActivitiesFilePath(), QSettings::IniFormat);
	QStringList topLevel = recentActivity.childGroups();

	if (topLevel.count())
	{
		topLevel = CPrime::SortFunc::sortDate(topLevel, Qt::DescendingOrder);
	}

	QListWidgetItem *item;

	Q_FOREACH (QString group, topLevel)
	{
		recentActivity.beginGroup(group);
		QStringList keys = recentActivity.childKeys();
		keys = CPrime::SortFunc::sortTime(keys, Qt::DescendingOrder, "hh.mm.ss.zzz");

		Q_FOREACH (QString key, keys)
		{
			QStringList value = recentActivity.value(key).toString().split("\t\t\t");
			if (value[0] == "corepdf")
			{
				QString filePath = value[1];
				if (not CPrime::FileUtils::exists(filePath))
				{
					continue;
				}

				QString baseName = CPrime::FileUtils::baseName(filePath);
				item = new QListWidgetItem(baseName);
				item->setData(Qt::UserRole, filePath);
				item->setToolTip(filePath);
				item->setIcon(CPrime::ThemeFunc::getFileIcon(filePath));
				ui->activitiesList->addItem(item);
			}
		}

		recentActivity.endGroup();
	}
}


void corepdf::on_menu_clicked()
{
	if (ui->sideView->isVisible())
	{
		ui->sideView->setVisible(false);
	}
	else
	{
		ui->sideView->setVisible(true);
	}
}

void corepdf::on_shareIt_clicked()
{
	if (!workFilePath.isNull())
	{
		ShareIT *t = new ShareIT(QStringList() << workFilePath, listViewIconSize, nullptr);
		// Set ShareIT window size
		if (uiMode == 2)
		{
			t->setFixedSize(QGuiApplication::primaryScreen()->size() * .8);
		}
		else
		{
			t->resize(500, 600);
		}

		t->exec();
	}
}


void corepdf::on_pinIt_clicked()
{
	if (!workFilePath.isNull())
	{
		PinIT *pit = new PinIT(QStringList() << workFilePath, this);
		pit->exec();
	}
}


void corepdf::openFileDialog()
{
	QString openFrom = QDir::homePath();

	if (!workFilePath.isNull())
	{
		openFrom = workFilePath;
	}
	QString fn = QFileDialog::getOpenFileName(
		this,
		"CorePDF - Open File",
		openFrom,
		"PDF Files (*.pdf);;All Files (*.*)"
		);

	if (not fn.isEmpty())
	{
		loadFile(fn);
	}
}


void corepdf::on_openmetadataApp_clicked()
{
	CPrime::AppOpenFunc::defaultAppEngine(CPrime::DefaultAppCategory::MetadataViewer, QFileInfo(workFilePath), QString());
}


void corepdf::on_savePageAsPic_clicked()
{
	int   pageNo = PdfWidget->pageNavigation()->currentPage();
	QSize pSize  = PdfDoc->pageSize(pageNo).toSize();

	/* Obtain the page zoom factor */
	QInputDialog *zoomDlg = new QInputDialog(this);

	zoomDlg->setInputMode(QInputDialog::IntInput);
	zoomDlg->setLabelText(
		QString(
			"Size of page %1 is %2 x %3 px. Please set the zoom factor below if necesssary."
			).arg(pageNo + 1).arg(pSize.width()).arg(pSize.height())
		);
	zoomDlg->setIntRange(10, 400);
	zoomDlg->setIntStep(10);
	zoomDlg->setIntValue(100);

	Q_FOREACH (QSpinBox *sb, zoomDlg->findChildren<QSpinBox *>())
	{
		if (sb)
		{
			sb->setSuffix(" %");
		}
	}

	if (zoomDlg->exec() == QDialog::Accepted)
	{
		/* Obtain the save file name */
		QStringList fmts;
		Q_FOREACH (QByteArray fmtba, QImageWriter::supportedImageFormats())
		{
			QString fmt = QString::fromLatin1(fmtba);
			fmts << fmt.toUpper() + " Image (*." + fmt + ")";
		}

		QString *str = new QString("PNG Image (*.png)");
		QString fn   = QFileDialog::getSaveFileName(
			nullptr,
			"CorePDF - Save Current Page",
			QDir::homePath() + "/untitled.png",
			fmts.join(";;") + ";;All Files (*.*)",
			str
			);

		if (not fn.isEmpty())
		{
			QString fmt = str->split(" ")[0].toLower();
			if (not fn.endsWith(fmt))
			{
				fn += "." + fmt;
			}

			qDebug() << "Requesting image of size" << pSize * zoomDlg->intValue() << fn;
			PageSaver *saver = new PageSaver(PdfDoc, pageNo, pSize * zoomDlg->intValue() / 100.0, fn);
			saver->start();
		}
	}
}


void corepdf::on_rotate_clicked()
{
	QDocumentRenderOptions rOpts = PdfWidget->renderOptions();

	int rotation = (( int )rOpts.rotation() + 1) % 4;

	rOpts.setRotation(( QDocumentRenderOptions::Rotation )rotation);

	PdfWidget->setRenderOptions(rOpts);
	PdfWidget->viewport()->update();
}


void corepdf::updateSearchLabel()
{
	QPair<int, int> srchPos = PdfWidget->getCurrentSearchPosition();

	searchCountLbl->setText(QString("Search %1 of %2").arg(srchPos.first).arg(srchPos.second));
}


void corepdf::on_activitiesList_itemDoubleClicked(QListWidgetItem *item)
{
	QString filepath = item->data(Qt::UserRole).toString();

	loadFile(filepath);
}


void corepdf::on_search_clicked()
{
    if(ui->searchB->isVisible()){
        ui->searchB->setVisible(false);
        PdfWidget->clearSearch();
    } else{
        ui->searchB->setVisible(true);
    }
}

void corepdf::on_closeSearch_clicked()
{
    ui->searchB->setVisible(false);
    PdfWidget->clearSearch();
}

void corepdf::on_previousW_clicked()
{
    if(!ui->searchHere->text().isEmpty()){
        PdfWidget->highlightPreviousSearchInstance();
    }
}

void corepdf::on_nextW_clicked()
{
    if(!ui->searchHere->text().isEmpty()){
        PdfWidget->highlightNextSearchInstance();
    }
}

void corepdf::jumpPage()
{
	int curPage  = PdfWidget->pageNavigation()->currentPage();
	int maxPages = PdfDoc->pageCount();

	QInputDialog *inDlg = new QInputDialog(this);

	inDlg->setWindowTitle("CorePDF | Goto Page");
	inDlg->setLabelText("Enter the page to which you want to jump:");
	inDlg->setInputMode(QInputDialog::IntInput);
	inDlg->setIntRange(1, maxPages);
	inDlg->setIntValue(curPage + 1);
	inDlg->setIntStep(1);

	QSpinBox *sb = inDlg->findChild<QSpinBox *>();

	if (sb)
	{
		sb->setSuffix(QString(" of %1 pages").arg(maxPages));
	}

	if (inDlg->exec())
	{
		int tgtPg = inDlg->intValue() - 1;
		PdfWidget->pageNavigation()->setCurrentPage(tgtPg);
	}
}

void corepdf::on_pageFit_clicked()
{
    pageFit = ( pageFit + 1 ) % 3;
    setupPage();
    smi->setValue("CorePDF", "PageFit", pageFit);
}


void corepdf::on_pageLayout_clicked()
{
    pageLayout = ( pageLayout + 1 ) % 3;
    setupPage();
    smi->setValue("CorePDF", "PageLayout", pageLayout);
}


void corepdf::on_pageView_clicked()
{
    pageView = ( pageView + 1 ) % 2;
    setupPage();
    smi->setValue("CorePDF", "PageView", pageView);
}

PageSaver::PageSaver(QDocument *doc, int page, QSize size, const QString& saveName)
	: mDoc(doc)
	, pageNo(page)
	, mSize(size)
	, imgName(saveName)
{
}


void PageSaver::run()
{
	QImage image = mDoc->renderPage(pageNo, mSize, QDocumentRenderOptions());

	if (not image.isNull())
	{
		QImage page(image.size(), QImage::Format_ARGB32);
		page.fill(Qt::white);

		QPainter p(&page);
		p.drawImage(QRect(QPoint(0, 0), page.size()), image);
		p.end();

		bool ret = page.save(imgName);

		qDebug() << "Saved file" << (ret ? "[SUCCESS]" : "[FAILED]");
		qDebug() << "Image" << image.size();
	}
}


