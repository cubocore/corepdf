/**
  * This file is a part of CorePDF.
  * A PDF viewer for C Suite.
  * Copyright 2019 CuboCore Group
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 3 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, vsit http://www.gnu.org/licenses/.
  **/

#pragma once

#include <QWidget>
#include <QThread>

class QDocument;
class QDocumentView;

class QListWidgetItem;
class QLabel;
class QBasicTimer;
class settings;

namespace Ui {
class corepdf;
}

class corepdf : public QWidget
{
	Q_OBJECT

public:
	explicit corepdf(QWidget *parent = nullptr);
	~corepdf();

	void loadFile(const QString& path);

	QString workFilePath;

protected:
    void timerEvent(QTimerEvent *) override;
    void closeEvent(QCloseEvent *event) override;

private slots:
	void on_menu_clicked();
	void on_shareIt_clicked();
	void on_pinIt_clicked();
	void on_openmetadataApp_clicked();
    void on_savePageAsPic_clicked();
	void on_rotate_clicked();
	void updateSearchLabel();
	void openFileDialog();
	void on_activitiesList_itemDoubleClicked(QListWidgetItem *item);
	void on_search_clicked();
	void on_closeSearch_clicked();
	void jumpPage();
    void on_previousW_clicked();
    void on_nextW_clicked();


private:
	Ui::corepdf *ui;
	QDocumentView *PdfWidget;
	QDocument *PdfDoc;
	settings *smi;
	QSize toolsIconSize, listViewIconSize, windowSize;
    bool activities, windowMaximized;
    int pageView, pageLayout, pageFit, uiMode;
	QAction *contLytAct;
	QLabel *searchCountLbl;

	/** Current zoom index */
	int cur_zoom_idx = 0;

	/** Search delay timer */
	QBasicTimer *delayTimer;

	void startsetup();
	void loadSettings();
	void loadActivities();
    void shotcuts();
	void setupIcons();
    void setupPage();

private Q_SLOTS:
    void on_pageFit_clicked();
    void on_pageLayout_clicked();
    void on_pageView_clicked();
};

class PageSaver : public QThread {
	Q_OBJECT

public:
	PageSaver(QDocument *doc, int page, QSize size, const QString& saveName);

private:
	QDocument *mDoc;
	int pageNo;
	QSize mSize;
	QString imgName;

protected:
	void run() override;
};
