/**
  * This file is a part of CorePDF.
  * A PDF viewer for C Suite.
  * Copyright 2019 CuboCore Group
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 3 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, vsit http://www.gnu.org/licenses/.
  **/


#include "corepdf.h"
#include <signal.h>

#include <QApplication>
#include <QCommandLineParser>
#include <QFileInfo>

int main(int argc, char **argv)
{
	QApplication app(argc, argv);

	// Set application info
	app.setOrganizationName("CuboCore");
	app.setApplicationName("CorePDF");
	app.setApplicationVersion(QStringLiteral(VERSION_TEXT));
	app.setDesktopFileName("cc.cubocore.CorePDF");
	app.setQuitOnLastWindowClosed(true);

	QCommandLineParser parser;

	parser.addHelpOption();         // Help
	parser.addVersionOption();      // Version

	/* Optional: Path where the search begins */
	parser.addPositionalArgument("paths", "Paths of the PDF documents which need to be opened.");

	/* Process the CLI args */
	parser.process(app);

	corepdf cpdf;

	cpdf.show();

	if (parser.positionalArguments().count() >= 1)
	{
		QFileInfo fi(parser.positionalArguments().at(0));
		if (fi.exists())
		{
			cpdf.loadFile(fi.absoluteFilePath());
		}
	}

	return app.exec();
}
