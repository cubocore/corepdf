/**
  * This file is a part of CorePDF.
  * A PDF viewer for C Suite.
  * Copyright 2019 CuboCore Group
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 3 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, vsit http://www.gnu.org/licenses/.
  **/


#include <QFileInfo>
#include <QDir>
#include <QStandardPaths>
#include <QFontDatabase>

#include <cprime/variables.h>
#include <cprime/themefunc.h>
#include <cprime/filefunc.h>

#include <sys/types.h> // getpwuid()
#include <pwd.h>       // getpwuid()
#include <unistd.h>    // getuid()

#include "settings.h"

settings::settings()
{
	defaultSett = QDir(CPrime::Variables::CC_Library_ConfigDir()).filePath("coreapps.conf");
	cSetting    = new QSettings(defaultSett, QSettings::NativeFormat);

	// set some default settings that are user specific
	if (!CPrime::FileUtils::exists(defaultSett))
	{
		qDebug() << "Settings file " << cSetting->fileName();
		CPrime::FileUtils::setupFolder(CPrime::FolderSetup::ConfigFolder);
	}

	checkAndSetCSuiteEntries(*cSetting);
	setAppDefaultSettings(*cSetting);
}


settings::~settings()
{
	delete cSetting;
}


// Check CSuite's default settings
void settings::checkAndSetCSuiteEntries(QSettings& settings)
{
	// Check if "CoreApps/KeepActivities" exists
	if (!settings.contains("CoreApps/KeepActivities"))
	{
		settings.setValue("CoreApps/KeepActivities", true);
	}

	// Check if "CoreApps/EnableExperimental" exists
	if (!settings.contains("CoreApps/EnableExperimental"))
	{
		settings.setValue("CoreApps/EnableExperimental", false);
	}

	// Check if "CoreApps/AutoDetect" exists
	if (!settings.contains("CoreApps/AutoDetect"))
	{
		settings.setValue("CoreApps/AutoDetect", true);
	}

	// Check if "CoreApps/DisableTrashConfirmationMessage" exists
	if (!settings.contains("CoreApps/DisableTrashConfirmationMessage"))
	{
		settings.setValue("CoreApps/DisableTrashConfirmationMessage", false);
	}

	if (autoUIMode() == 2)
	{
		// Check if "CoreApps/IconViewIconSize" exists
		if (!settings.contains("CoreApps/IconViewIconSize"))
		{
			settings.setValue("CoreApps/IconViewIconSize", QSize(56, 56));
		}

		// Check if "CoreApps/ListViewIconSize" exists
		if (!settings.contains("CoreApps/ListViewIconSize"))
		{
			settings.setValue("CoreApps/ListViewIconSize", QSize(48, 48));
		}

		// Check if "CoreApps/ToolsIconSize" exists
		if (!settings.contains("CoreApps/ToolsIconSize"))
		{
			settings.setValue("CoreApps/ToolsIconSize", QSize(48, 48));
		}
	}
	else
	{
		// Check if "CoreApps/IconViewIconSize" exists
		if (!settings.contains("CoreApps/IconViewIconSize"))
		{
			settings.setValue("CoreApps/IconViewIconSize", QSize(48, 48));
		}

		// Check if "CoreApps/ListViewIconSize" exists
		if (!settings.contains("CoreApps/ListViewIconSize"))
		{
			settings.setValue("CoreApps/ListViewIconSize", QSize(32, 32));
		}

		// Check if "CoreApps/ToolsIconSize" exists
		if (!settings.contains("CoreApps/ToolsIconSize"))
		{
			settings.setValue("CoreApps/ToolsIconSize", QSize(24, 24));
		}
	}

	cSetting->sync();
}


// Check app's default settings
void settings::setAppDefaultSettings(QSettings& settings)
{
    // Check if "CorePDF/PageFit" exists
    if (!settings.contains("CorePDF/PageFit"))
	{
        settings.setValue("CorePDF/PageFit", 1);
	}

	// Check if "CorePDF/PageLayout" exists
	if (!settings.contains("CorePDF/PageLayout"))
	{
        settings.setValue("CorePDF/PageLayout", 1);
	}

    // Check if "CorePDF/PageView" exists
    if (!settings.contains("CorePDF/PageView"))
	{
        settings.setValue("CorePDF/PageView", 1);
	}

	// Check if "CorePDF/WindowSize" exists
	if (!settings.contains("CorePDF/WindowSize"))
	{
		settings.setValue("CorePDF/WindowSize", QSize(800, 500));
	}

	// Check if "CorePDF/WindowMaximized" exists
	if (!settings.contains("CorePDF/WindowMaximized"))
	{
		settings.setValue("CorePDF/WindowMaximized", false);
	}

	cSetting->sync();
}


int settings::autoUIMode() const
{
	if (CPrime::ThemeFunc::getFormFactor() == CPrime::FormFactor::Mobile)
	{
		return 2; // Mobile
	}
	else if ((CPrime::ThemeFunc::getFormFactor() == CPrime::FormFactor::Tablet) && (CPrime::ThemeFunc::getTouchMode() == true))
	{
		return 1; // Tablet
	}
	else
	{
		return 0; // Desktop
	}
}


settings::cProxy settings::getValue(const QString& appName, const QString& key,
									const QVariant& defaultValue)
{
	if ((appName == "CoreApps") && (key == "UIMode")) // Wants to get CoreApps/UIMode
	// Check whether CoreApps/AutoDetect is On
	{
		bool isAutoDetect = cSetting->value("CoreApps/AutoDetect").toBool();

		if (isAutoDetect)
		{
			return cProxy { cSetting, "Dummy", autoUIMode() };
		}
	}

	return cProxy{ cSetting, appName + "/" + key, defaultValue };
}


void settings::setValue(const QString& appName, const QString& key, QVariant value)
{
	cSetting->setValue(appName + "/" + key, value);
}


QString settings::defaultSettingsFilePath() const
{
	return cSetting->fileName();
}
